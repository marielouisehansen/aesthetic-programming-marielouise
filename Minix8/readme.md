
![screenshot](positive1.png) ![screenshot](positive2.png)


https://marielouisehansen.gitlab.io/aesthetic-programming-marielouise/Minix8/



Questions to think about
– Provide a title of your work and a short description (1,000 characters or less).
– Describe how your program works, and what syntax you have used, and learnt?

Positivity 

Positivity is a codework that focus on the good events in the year of 2020. When the work is opened, the user will see a yellow canvas with the text ‘A positive thing that happened in 2020:’. Beneath the text, random events will be shown with the speed of 60 frames pr. second. When the user presses the canvas, the frame rate will become to 1 frame pr. second, making it easier to read the events. 

In this miniX, we have focused on coding with a JSON file (events.json). We have learned that it is important to upload the JSON file (function preload) in order to work with the file. The pathway of a JSON-file is very important as well. In the beginning of the syntax the name of the JSON file is written and the array ‘events’ is referred to by using 0, since it’s the first section in the JSON file (json[0]). Then the word of the array events is written, referring to the whole array. If we would like to recall the first sentence, we would write events[0] instead.   

Analysis: 
According to Soon and Cox, code cannot only be seen as a way of giving a computer a list of actions, but code can also be seen as lyrics: 

“Software is constructed from language, and is processed with and via computational procedures consisting of source code as symbols. Code is like poetry then, inasmuch as it plays with language’s structures, setting up temporal interplay between the “voice” that is, and the “voice” that is to come.” (Soon & Cox, 2020, p.167)

As Soon and Cox writes, Code is constructed from the human language, making it easier for the programmer to understand the given order. But since code is a language, it also opens up opportunities for making it like poetry. 
   Soon and Cox describes this type of poetry as ‘vocable code’ in the 7th chapter of their book ‘Aesthetic Programming: A Handbook of Software Studies’. Vocable code is code that cannot simply be reduced to something purely functional. This means that vocable code is not only software art, but also codework. Codework is a form of poetry using high-level programming languages. 

While our program has been written using a high-level programming language, we have not focused much on integrating poetry into the source code, but rather on creating and understanding the coding of a json file. Even though the code doesn’t include poetry like elements, the user would still be able to understand the meaning of the program from looking at the source code. Our json file contains events (it is also named events.json) that are positive. It is written in the source code (minix8sketch.js) line 25: “happy = json[0].events[1];” that “happy” refers to the json file containing the different events. Therefore a potential reader of the code would be able to tell that our intentions were to bring happy events into our program. 


Sources:

https://www.purewow.com/family/good-things-that-happened-in-2020

https://www.huffpost.com/entry/good-things-happened-2020_l_5feb660fc5b6ff7479847494

https://eu.usatoday.com/story/life/2020/07/29/100-good-things-from-2020-positive-stories-news/3257222001/

http://amiraha.com/mexicansincanada/


