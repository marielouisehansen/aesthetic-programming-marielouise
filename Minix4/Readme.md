
![screenshot](question.png) ![screenshot](answer.png)

https://marielouisehansen.gitlab.io/aesthetic-programming-marielouise/Minix4/

title: The computer already knows

For this minix I got my inspiration from the documentary about surveillance capitalism. I wanted to create something in the spirit of software and algorithmic knowledge about the users. In the documentary, it was mentioned that at one point, a woman started seeing commercials for baby products. It turned out that the algorithm knew that she was pregnant – even before she did herself. To me, that is absolutely crazy! It scares me to know that society is so intertwined in SoMe now, and that it is hard to live a ‘normal’ life without using it. We are almost forced to give ourselves to the computers and the people behind the big tech companies. 
As mentioned, I drew inspiration from that documentary. I decided to have the user sort of ‘log’ their personal emotions in my program, and then be told that the computer already knew. My program doesn’t save the answers, and no matter what button the user presses, the answer will be the same. So my program is just supposed to symbolize how today’s computers basically know you to your very core, even though it doesn’t feel like it. And in the case of the women mentioned earlier, the computer even figured out her pregnancy before she did herself. Crazy. I wanted to sort of bring some of that into my program, despite my very limited programming skills. 
