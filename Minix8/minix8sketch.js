let json;
let happy;


function preload(){
  json = loadJSON('events.json');
}


function setup(){
  createCanvas(windowWidth, windowHeight);

}

 function draw(){

background(255, 248, 132);

textStyle(BOLD);
textSize(48);
textAlign(CENTER);
 text('A positive thing that happened in 2020:', windowWidth/2, 100);

 happy = json[0].events[1];
 textAlign(CENTER);
 textSize(24);
 text(random(json[0].events),windowWidth/2,300);

   if (mouseIsPressed){
    frameRate(0.5);
  } else {
    frameRate(60);
  }
}


// Notes for the json file:
// we have to put squar bracets around the json, due to a default in chrome
