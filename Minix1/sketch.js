let Candy;

function preload() {
Candy = loadImage("Candy01.png");
}

function setup () {
createCanvas(640, 480);
background('#fae');
  }

  function draw() {
    fill(255);
    textSize(23);
    textStyle(BOLD);
    text('Welcome to candyland! Have a click around!', 85, 47);
  }

function mousePressed() {
imageMode(CENTER);
  image(Candy, mouseX, mouseY, 90, 90);

}
