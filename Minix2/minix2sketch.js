let mandalas;

function preload() {
mandalas = loadImage("Mandalas.png");

}

function setup () {
createCanvas(windowWidth, windowHeight);
background('#E6D591');

  }

  function draw() {
  fill(000000);
  textSize(20);
   textStyle(BOLD);
   text('MY TWO MOODS THIS WEEK WERE', 150, 40);

   circle(140, 150, 10);
   circle(170, 150, 10);

noFill();
   curve(100,100,170,170,150,170,150,-10);

fill(000000);
   circle(500,200,10);
   circle(540,200,10);

   noFill();
    line(530, 230, 500, 230);

    fill(000000);
    textSize(20);
     textStyle(BOLD);
     text('CLICK TO SEE A PHOTO OF SOMETHING THAT MADE ME HAPPY THIS WEEK!', 100, 500);
}

     function mousePressed() {
     imageMode(CENTER);
    image(mandalas, mouseX, mouseY, 500, 500);

    fill(000000);
    textSize(30);
     textStyle(BOLD);
     text('I finished coloring this mandala! yay.', 500, 100);


  }
