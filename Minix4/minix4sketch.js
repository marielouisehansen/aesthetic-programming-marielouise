let thankyou;

function preload() {
thankyou = loadImage("thankyou.png");

}

function setup() {
createCanvas(windowWidth, windowHeight);
background('#fae');
}

function draw() {
  fill(000000);
  textSize(20);
  textStyle(BOLD);
   text('How do you feel?', 150, 90);

//Now im creating the buttons with emotions written on them
  button1 = createButton("Happy");
  button1.position(100, 130);
  button1.mousePressed(thankyouLoad);

  button2 = createButton("Sad");
  button2.position(100, 150);
  button2.mousePressed(thankyouLoad);

  button3 = createButton("Hopeful");
  button3.position(100, 170);
  button3.mousePressed(thankyouLoad);

  button4 = createButton("Hopeless");
  button4.position(100, 190);
  button4.mousePressed(thankyouLoad);

  button5 = createButton("Some other feeling");
  button5.position(100, 210);
  button5.mousePressed(thankyouLoad);
}

function thankyouLoad() {
  fill(000000);
  textSize(20);
  textStyle(BOLD);
   text('Thats what we thought.', 300, 150);

}
