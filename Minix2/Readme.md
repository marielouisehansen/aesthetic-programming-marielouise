
![screenshot](mood1.png) ![screenshot](mood2.png)


Minix2 sketch (https://marielouisehansen.gitlab.io/aesthetic-programming-marielouise/Minix2/)

This week I decided to do just the bare minimum. I figured I could draw my two main moods of the week: happy-ish and meh. And then in addition to that, I decided to add a picture of a mandala that I filled out, because I think it looks pretty . 

I realize that this program is not fun for the user, and I would like to apologize for that. 

In regard to putting my emojis in a wider social and cultural context, I don’t really know what to say. I tried to mimic the old school “:)” and “:l” smiley faces. As for the background color, I chose a beige-ish color because it is boring (I saw an episode of some tv show once, where one of the characters was dressed in beige from head to toe in order to symbolize his super boring personality) – which I thought would be fitting because I have been feeling super meh this week. 
