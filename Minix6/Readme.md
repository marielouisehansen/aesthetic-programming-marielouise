
![screenshot](universe.png)

Title: Space

https://marielouisehansen.gitlab.io/aesthetic-programming-marielouise/Minix6/


1.	What have you changed and why?
Note: I decided to work on my Minix5 (generative program)
I have changed my program by adding buttons in order to give some more control to the user as far as ‘drawing’ goes, as well as added some dots into the visuals. One button controls when lines are added, while the other button restarts the program. 

2.	How would you demonstrate aesthetic programming in your work?
I think aesthetic programming is a quite hard idea to understand and fully comprehend. While my current understanding of the subject is centered around programming more than or something else than something “purely” functional. I would like to have a message with my program that relates to society. Some sort of critical remark. I would have liked to do something that relates to the current (and very important) debate about female safety in public at night, and just female safety in general. That is something that would, to me, be a good subject for aesthetic programming – to comment on something in society with my work. This miniX is inspired by the night sky. It is supposed to look like a galaxy with a collapsing star in the center. When the user clicks the button, the star collapses further. I came up with the idea because I get a lot of tiktoks about space and zodiac signs, which are of course not the exact same. But nevertheless, they both revolve around space. So that’s what I wanted to bring out through my program. 

3.	What does it mean by programming as a practice, or even as a method for design?
To me, it means embracing this new world we live in where everything is digital. I think it shows how truly creative humans can be, to be able to take something so scientific and still be able to use it as a way to treasure human emotion. 

4.	What is the relation between programming and digital culture?
Digital culture forces us to be present online, so it is quite nice to have an understanding of systems such as algorithms, that can be used to influence us. If it was up to me, everyone should have the opportunity to learn basic programming, as it gives an understanding of computers (which are literally everywhere at this point), as well as how computers can be used for both good and less good. 

5.	draw and link some of the concepts in the text Aesthetic Programming
It is stated in the text how aesthetics in aesthetic programming do not only refer to the visuals, but also to the societal or political. My program stems from my fascination with space, which has been a staple of humanity since the beginning. Whether the stars were used for direction, to track the seasons or viewed as gods, the cosmos has always been of great significance to humans. Therefore I wanted to create something inspired by space, and figured that a generative program would be quite cool since space is out of our control, but still has to (at least in our knowledge) confine to some laws of physics, just like this program has to confine to the rules in the code. 
